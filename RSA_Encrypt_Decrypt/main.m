//
//  main.m
//  RSA_Encrypt_Decrypt
//
//  Created by sban@netspectrum.com on 9/25/12.
//  Copyright (c) 2012 sban@netspectrum.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
